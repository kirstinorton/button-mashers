How to Play
	Controls:
		-Right Arrow key = move right
		-Left Arrow Key = move left
		-space bar = jump
		-x key = kick
		-z key for punch
		
	Goal: Diminish the other player's health bar to zero before the timer runs out.
	
Milestone 4 Plan:
	Backlog Items to Implement:
	-Beautify the layouts, including the timer, health and mash bars.
	-Add animations for button mashing special attacks.
	-Tweak the health amounts and the timer to values that make the gameplay better.
	-Add the ability to block attacks.
	-Add Marathon mode to game.	
	
Repository information
	-We have a Bitbucket repository for our game. 
	-The url for it is https://bitbucket.org/kirstinorton/button-mashers

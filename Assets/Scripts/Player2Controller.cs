﻿using UnityEngine;
using System.Collections;

public class Player2Controller : MonoBehaviour {
    
    public float maxSpeed = 5f;
    public float jumpMax = 700f;
    
    bool grounded = true;
    bool punchDamageFlag = false;
    bool kickDamageFlag = false;
    bool comboDamageFlag = false;
    GameObject opponent; 
    PlayerHealth opponentHealth;
    ComboBar comboBar;
    
    Animator anime;
    private int idleState;// = Animator.StringToHash(anime.GetLayerName(0)+".Idle");
    private int currentAnimState;
    
    // Use this for initialization
    void Start () {
        opponent = GameObject.FindGameObjectWithTag ("Player");
        opponentHealth = opponent.GetComponent <PlayerHealth> ();
        comboBar = GetComponent <ComboBar> ();
        Flip();
        anime = GetComponent<Animator> ();
        
        anime.SetBool("punch", false);    
        anime.SetBool("kick", false);
        idleState = Animator.StringToHash(anime.GetLayerName(0)+".Idle");
        
        if (GameObject.Find("CharPrefs")) {
            Debug.Log("Using Selection Prefs");
            string[] chars = {"Gundam_Animation","Capt_Anim_Cntrl","Goku_Animation","Ryu_Anim_Cntrl"};
            int characterInd = GameObject.Find("CharPrefs").GetComponent<CharacterValues>().OppChar;
            anime.runtimeAnimatorController = Resources.Load<RuntimeAnimatorController>(chars[characterInd]);
            Debug.Log("Difficulty int: " + GameObject.Find("CharPrefs").GetComponent<CharacterValues>().Diff);
        }
    }
    
    void Update(){
        if (grounded && Input.GetKeyDown(KeyCode.I)) {
            if (Input.GetKeyDown(KeyCode.Space) && grounded) {
                grounded = false;
                Rigidbody2D body = this.rigidbody2D;
                body.AddForce(new Vector2(0, jumpMax * 4));
                //grounded = false;
                
            }
        }
        damageFlagHandler();
        inputHandler();
        
    }
    private string targetState = "";
    void inputHandler(){
        
        BoxCollider2D hitBox = transform.GetChild(0).collider2D as BoxCollider2D;
        hitBox.size = new Vector2(.2f, .24f);
        currentAnimState = anime.GetCurrentAnimatorStateInfo(0).nameHash;
        if(Input.GetKeyDown(KeyCode.O)){
            targetState = "Punch";
            animHandler(targetState);
            hitBox.size = new Vector2(.5f, .24f);
        }
        if(Input.GetKeyDown(KeyCode.P)){
            targetState = "Kick";
            animHandler(targetState);
            hitBox.size = new Vector2(.5f, .24f);
        }
        //blocking = c press
    }
    void damageFlagHandler(){
        currentAnimState = anime.GetCurrentAnimatorStateInfo(0).nameHash;
        if(currentAnimState == getStateHash("Idle") || currentAnimState == getStateHash("Walk")){
            punchDamageFlag = false;
            kickDamageFlag = false;
        }
    }
    void animHandler(string state){
        if(comboBar.comboReady()){
            state = "Mash";
        }
        //get the Animator state info from the base layer
        switch(state){
            case "Punch":
                if(currentAnimState!=getStateHash(state)){
                    anime.Play(state);
                    punchDamageFlag = true;
                }
                break;
            case "Kick":
                if(currentAnimState==idleState||currentAnimState==getStateHash("Walk")){
                    anime.Play(state);
                    kickDamageFlag = true;
                }
                break;
            case "Mash":
                if(currentAnimState!=getStateHash(state)){
                    anime.Play(state);
                    comboDamageFlag = true;
                }
                break;
        }
    }
    int getStateHash(string state){
        return Animator.StringToHash(anime.GetLayerName(0)+'.'+state);
    }
    void OnCollisionEnter2D(Collision2D col){
        if(col.collider.name == "Floor"){
            grounded = true;
        }
    }
    void FixedUpdate(){ //physics code
        if (grounded) { //disable moving left or right while in the air*****************
            float move = Input.GetAxis ("Horizontal2");
            Rigidbody2D body = this.rigidbody2D;
            body.velocity = new Vector2 (move * maxSpeed, body.velocity.y); 
            anime.SetFloat("walk", Mathf.Abs(move) * maxSpeed);
        }
        
    }
    void Flip(){    //function used to flip sprite into opposite direction
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;    
    }
    void OnTriggerStay2D(Collider2D col){

        if(comboDamageFlag){
            print("COMBO");
            comboDamageFlag = false;
            opponentHealth.TakeDamage(15);
            comboBar.Reset();
        }
        if(punchDamageFlag){
            print("Player punched opponent");
            punchDamageFlag = false;
            opponentHealth.TakeDamage (1);
            comboBar.Mash();
        }
        if(kickDamageFlag){
            print("Player kicked opponent");
            kickDamageFlag = false;
            opponentHealth.TakeDamage (2);
            comboBar.Mash();
        }
    }
    
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ComboBar : MonoBehaviour {
	public int currentComboValue;
	public Slider comboBar;                                 // Reference to the UI's combo bar.
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public bool comboReady(){
		return(currentComboValue == comboBar.maxValue);
	}
	public void Mash ()
	{
		currentComboValue++;
		if(currentComboValue <= comboBar.maxValue){
			comboBar.value = currentComboValue;
		}else{
			//COMBO!!!!
		}
	}
	
	public void Reset ()
	{
		currentComboValue=0;
		comboBar.value = currentComboValue;		
	}
}

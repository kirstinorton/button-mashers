﻿using UnityEngine;
using System.Collections;

public class CharacterValues : MonoBehaviour {
	//0 - gundam
	//1 - Captain America
	//2 - Goku
	//3 - Ryu
	public int PlayerChar {
		get;
		set;
	}
	public int OppChar {
		get;
		set;
	}
    public int Diff {
        get;
        set;
    }
    public int CPU {
        get;
        set;
    }
	void Awake() {
		DontDestroyOnLoad(this);
	}
}
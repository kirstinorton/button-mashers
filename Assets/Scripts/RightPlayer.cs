﻿using UnityEngine;
using System.Collections;

public class RightPlayer : MonoBehaviour {

	// Use this for initialization
	void Start () {
        if (GameObject.Find("CharPrefs") != null) {
            if (GameObject.Find("CharPrefs").GetComponent<CharacterValues>().CPU == 1) {
                gameObject.AddComponent("Player2Controller");
            } else {
                gameObject.AddComponent("OpponentController");
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

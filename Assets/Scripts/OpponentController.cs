using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OpponentController : MonoBehaviour {
	public float moveRate = 1f;
	public int xDirection = -1; //forward

	Animator anime;
	Animator playerAnime;
	GameObject player; 
	PlayerHealth playerHealth;
	OpponentHealth AIHealth;
	ComboBar comboBar;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
		playerHealth = player.GetComponent <PlayerHealth> ();
		AIHealth = this.GetComponent<OpponentHealth> ();
		comboBar = GetComponent <ComboBar> ();

		Flip ();
		anime = GetComponent<Animator> ();

        if (GameObject.Find("CharPrefs")) {
            //Debug.Log("Using Selection Prefs");
            string[] chars = {"Gundam_Animation","Capt_Anim_Cntrl","Goku_Animation","Ryu_Anim_Cntrl"};
            int characterInd = GameObject.Find("CharPrefs").GetComponent<CharacterValues>().OppChar;
            anime.runtimeAnimatorController = Resources.Load<RuntimeAnimatorController>(chars[characterInd]);

        }
		SetupAI ();
	}

	void Update () {
		++frames;
		DetermineState ();
		previousPlayerLocation = player.transform.position.x;
	}

	void FixedUpdate(){}

	void OnCollisionEnter2D(Collision2D col){

		if(col.collider.tag == "Player"){
			attacked = true;
		}
		if(col.collider.tag == "rightwall"){
			hitWall = true;
			Debug.Log("collided with wall");
			if(difficulty == DIFFICULTY.BEGINNER){
				xDirection = forward;
			}
		}
		if(col.collider.name == "Floor"){
			grounded = true;
		}
	}

	void Flip(){	//function used to flip sprite into opposite direction
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;	
	}
    void OnTriggerEnter2D(){
        //Debug.Log("Opponent Was Hit");
    }

	void OnCollisionExit2D(Collision2D col){	//what happens when something exits the hitbox
		if(col.collider.tag == "Player"){
			attacked = false;
		}
		if (col.collider.tag == "rightwall") {
			hitWall = false;
			Debug.Log("uncollided with wall");
			xDirection = forward;
		}
	}

	private enum DIFFICULTY : int{ BEGINNER, MEDIUM, HARD };
	private enum STATES : int{ EVADE, ATTACK, SEEK, IDLE };
	private STATES AI_State = STATES.SEEK;

	private DIFFICULTY difficulty = DIFFICULTY.HARD;
	private bool attacked = false;
	private bool hitWall = false;
	private int attackDelay;
	private bool grounded = true;
	public float jumpMax = 110;
	private int frames = 0;
	private int pickAttackCounter = 1;
	private int pickEvasionCounter = 1;
	private float previousPlayerLocation;
	private int forward = -1;
	private int backward = 1;
	 
	void SetupAI(){	//sets up all the values for the AI
        difficulty = setAIDifficulty(GameObject.Find("CharPrefs").GetComponent<CharacterValues>().Diff); //*****************************************************
		switch(difficulty){
			case DIFFICULTY.BEGINNER: 	attackDelay = 65;			//attacks the slowest, and just tries to evade
										xDirection = backward;
										break;
			case DIFFICULTY.MEDIUM:		attackDelay = 45;			//medium fast attack, goes balls to the wall attacking
										break;
			case DIFFICULTY.HARD:		attackDelay = 40;			//fastest attack, makes the player come to it
										AI_State = STATES.IDLE;
										break;
			default:					break;
		}	
		previousPlayerLocation = player.transform.position.x;
	}
	void DetermineState(){
		switch (AI_State){
			case STATES.EVADE:	Evade(); 
								break;	
			case STATES.ATTACK:	Attack ();
								break;
			case STATES.SEEK:	Seek();
								break;
			case STATES.IDLE:	Idle();
								break;
			default: 			break;
		}
	}

	void Evade(){	//get away from opponent 
		//Debug.Log("Evade " + xDirection);
		float currentPlayerLocation = player.transform.position.x;
		switch(difficulty){
			case DIFFICULTY.BEGINNER: 	if (attacked) {	AI_State = STATES.ATTACK;	} 
										else{ 	
											xDirection = backward;
											PickEvasion();	
										}
                                        break;
			case DIFFICULTY.MEDIUM:		AI_State = STATES.SEEK;	//medium never evades
										break;
			case DIFFICULTY.HARD:		if (attacked) {
											AI_State = STATES.ATTACK;
										}
										else if(currentPlayerLocation == previousPlayerLocation){
											AI_State = STATES.IDLE;
										}
										else if(AIHealth.currentHealth >= playerHealth.currentHealth || comboBar.currentComboValue >= comboBar.comboBar.maxValue - 2){ 
											AI_State = STATES.SEEK;
											xDirection = forward;
											Debug.Log("moving away from wall");
											hitWall = false;
										}
										else{	PickEvasion();	}		//do nothing, stay here...
										break;
			default:					break;
		}

	}
	void Attack(){
		//Debug.Log("Attack");
		if(attacked){
			if (frames % attackDelay == 0) {	//can I attack?
				PickAttack();	//pick and apply attack
				frames = 0;
			}

		}
		switch(difficulty){
			case DIFFICULTY.BEGINNER: 	if(AIHealth.currentHealth < playerHealth.currentHealth){
											if(!hitWall){ xDirection = backward;}
											AI_State = STATES.EVADE;
										}
										break;
			case DIFFICULTY.MEDIUM:		if(!attacked){ AI_State = STATES.SEEK;}
										break;	
			case DIFFICULTY.HARD:		if(!attacked){
											if(comboBar.currentComboValue == comboBar.comboBar.maxValue){AI_State = STATES.SEEK;} //if my combo bar is full and I can survive the next hit
											else if(AIHealth.currentHealth >= playerHealth.currentHealth){AI_State = STATES.IDLE;}
											else {AI_State = STATES.EVADE;} //if my health is too low
										}
										break;
			default:					break;
		}
	}
	void Seek(){	//find opponent 
		//Debug.Log("Seek " + xDirection);
		float currentPlayerLocation = player.transform.position.x;
		xDirection = forward;
		switch(difficulty){
			case DIFFICULTY.BEGINNER: 	if (!attacked) {
											xDirection = forward;
											Move ();
										}
										else{	AI_State = STATES.ATTACK;	}
										break;
			case DIFFICULTY.MEDIUM:		if(attacked){	AI_State = STATES.ATTACK;}
										else{	
											xDirection = forward; 
											Move ();			
										}
										break;
			case DIFFICULTY.HARD:		if (attacked) {	AI_State = STATES.ATTACK;	}
										else if(AIHealth.currentHealth > playerHealth.currentHealth){ AI_State = STATES.IDLE;}
										else if(AIHealth.currentHealth < playerHealth.currentHealth && comboBar.currentComboValue != comboBar.comboBar.maxValue){ AI_State = STATES.EVADE;}
										else{
											xDirection = forward;							
											Move ();
										}
										break;
			default:					break;
		}
	}
	void Idle(){ 
		//Debug.Log("Idle");
		float currentPlayerLocation = player.transform.position.x;
		switch(difficulty){
			case DIFFICULTY.BEGINNER: 	AI_State = STATES.EVADE;	//beginner won't ever Idle just keep moving 
										break;
			case DIFFICULTY.MEDIUM:		if(frames % 123 == 0){ //idle for a bit then go seek for player
											AI_State = STATES.SEEK;
										}
										break;	
			case DIFFICULTY.HARD:		if(attacked){
											AI_State = STATES.ATTACK;
										}
										else if(previousPlayerLocation > currentPlayerLocation){ //add health check and combo check
											AI_State = STATES.SEEK;
										}
										else if(previousPlayerLocation < currentPlayerLocation){ 
											AI_State = STATES.EVADE;
										} else{} //stay in idle state
										break;
			default:					break;
		}
	}

	//Evade Helper Functions
	void Jump(){
		if (grounded) {
			Rigidbody2D body = this.rigidbody2D;
			body.AddForce(new Vector2(0, jumpMax * 4));
			grounded = false;
		}
	}
	void Move(){ 
		if(grounded){
			Rigidbody2D body = this.rigidbody2D;
			body.velocity = new Vector2 ((0.1f + moveRate) * xDirection, body.velocity.y);
			anime.Play("Walk");
		}
	}
	void PickAttack(){   
		bool kickFlag = false; //false for punch, true for kick
		switch(difficulty){
			case DIFFICULTY.BEGINNER:  	if(pickAttackCounter % 4 == 0){				//punch 75% kick 25%
											kickFlag = true;
										}
										break;	
			case DIFFICULTY.MEDIUM:		if(pickAttackCounter % 2 == 0){				//kick 50% punch 50%
											kickFlag = true;
										}
										break;	
			case DIFFICULTY.HARD:		if(pickAttackCounter % 4 != 0){				//punch 25% kick 75%
											kickFlag = true;
										}
										break;	
			default:					break;
		}

		if(comboBar.comboReady()){
			anime.Play("Mash");
			playerHealth.TakeDamage(15);
			comboBar.Reset();
		}
		else if (kickFlag) {
			anime.Play("Kick");
			playerHealth.TakeDamage(2);
			pickAttackCounter = 0;
		}
		else{
			anime.Play("Punch");
			playerHealth.TakeDamage(1);
		}
		comboBar.Mash ();
		++pickAttackCounter;
	}
	void PickEvasion(){
		//implement blocking in here!*********************8
		switch (difficulty) {
			case DIFFICULTY.BEGINNER:		if(pickEvasionCounter % 246 == 0){ //jump every once in a while...
												Jump();
												pickEvasionCounter = 1;
											}
											else{ Move(); }
											break;
				case DIFFICULTY.MEDIUM: 	break;
				case DIFFICULTY.HARD: 		xDirection = backward;
											if (hitWall){ 
												Jump(); 
												xDirection = forward;
											}
											
											Move ();
											
											break;
				default: 					break;
		}
		++pickEvasionCounter;
	}
	DIFFICULTY setAIDifficulty(int dif){ 
		switch (dif) {
			case 0: return DIFFICULTY.BEGINNER;	
			case 1: return DIFFICULTY.MEDIUM;
			case 2: return DIFFICULTY.HARD;
			default: return DIFFICULTY.BEGINNER;
		}
	}

}

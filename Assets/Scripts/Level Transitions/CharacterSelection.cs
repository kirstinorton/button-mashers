﻿using UnityEngine;
using System.Collections;

public class CharacterSelection : MonoBehaviour {
	int diffSelected = 1;
    int diffCpu = 0;
	void OnGUI() {
		Sprite[] gundam = (Resources.LoadAll<Sprite>("Gundam"));
        Sprite[] capAm = (Resources.LoadAll<Sprite>("Captain America"));
        Sprite[] goku = (Resources.LoadAll<Sprite>("DBZ Goku"));
        Sprite[] ryu = (Resources.LoadAll<Sprite>("Street Fighter Left"));
		var bools = GameObject.Find("CharPrefs").GetComponent<CharacterValues>();
		string[] difficulties = new string[] { "Easy", "Medium", "Hard" };
        string[] cpu = new string[] { "CPU", "Player 2"};
        Rect position = new Rect(350, 400, 400, 100);
        diffSelected = GUI.SelectionGrid(position, diffSelected, difficulties, difficulties.Length, GUI.skin.toggle);
        string[] playerCpiu = new string[] { "Easy", "Medium", "Hard" };
        Rect playCpuPos = new Rect(400, 500, 300, 100);
        diffCpu = GUI.SelectionGrid(playCpuPos, diffCpu, cpu, cpu.Length, GUI.skin.toggle);
		if (GUI.Button(new Rect(145, 10, 100, 100), new GUIContent("Gundam"))) {
			GameObject.Find("PlayerSprite").GetComponent<SpriteRenderer>().sprite = gundam[0];
			bools.PlayerChar = 0;
		}
		else if (GUI.Button(new Rect(250, 10, 100, 100), new GUIContent("Cap. America"))) {
			GameObject.Find("PlayerSprite").GetComponent<SpriteRenderer>().sprite = capAm[0];
			bools.PlayerChar = 1;
		}
		else if (GUI.Button(new Rect(145, 110, 100, 100), new GUIContent("Goku"))) {
			GameObject.Find("PlayerSprite").GetComponent<SpriteRenderer>().sprite = goku[0];
			bools.PlayerChar = 2;
		}
		else if (GUI.Button(new Rect(250, 110, 100, 100), new GUIContent("Ryu"))) {
			GameObject.Find("PlayerSprite").GetComponent<SpriteRenderer>().sprite = ryu[0];
			bools.PlayerChar = 3;
		}
		//Opponent
		else if (GUI.Button(new Rect(645, 10, 100, 100), new GUIContent("Gundam"))) {
            GameObject.Find("OppSprite").GetComponent<SpriteRenderer>().sprite = gundam[0];
			bools.OppChar = 0;
		}
		else if (GUI.Button(new Rect(750, 10, 100, 100), new GUIContent("Cap. America"))) {
            GameObject.Find("OppSprite").GetComponent<SpriteRenderer>().sprite = capAm[0];
			bools.OppChar = 1;
		}
		else if (GUI.Button(new Rect(645, 110, 100, 100), new GUIContent("Goku"))) {
            GameObject.Find("OppSprite").GetComponent<SpriteRenderer>().sprite = goku[0];
			bools.OppChar = 2;
		}
		else if (GUI.Button(new Rect(750, 110, 100, 100), new GUIContent("Ryu"))) {
            GameObject.Find("OppSprite").GetComponent<SpriteRenderer>().sprite = ryu[0];
			bools.OppChar = 3;
		}
		else if (GUI.Button(new Rect(300, 230, 400, 100), new GUIContent("Play"))) {
            bools.Diff = diffSelected;
            bools.CPU = diffCpu;
			Application.LoadLevel("MainEnvironment");
		}
	}

}
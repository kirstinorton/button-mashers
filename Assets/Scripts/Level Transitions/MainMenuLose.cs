﻿using UnityEngine;
using System.Collections;

public class MainMenuLose : MonoBehaviour {
	
	void OnGUI(){
		GUI.Box (new Rect(300,10,400,100), new GUIContent("Sorry you lost. Try again..."));
		if (GUI.Button (new Rect (300,110,400,100), new GUIContent ("Play"))) {
            Application.LoadLevel ("Character Selection");
		}
		if (GUI.Button (new Rect (300, 210, 400, 100), new GUIContent ("Quit"))) {
			Application.Quit();
		}
	}
}

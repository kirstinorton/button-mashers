﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {
	
	void OnGUI(){
		if (GUI.Button (new Rect (325,200, 300, 100), new GUIContent ("Play"))) {
			Application.LoadLevel ("Character Selection");
		}
		if (GUI.Button (new Rect (325, 320, 300, 100), new GUIContent ("Quit"))) {
			Application.Quit();
		}
	}
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OpponentHealth : MonoBehaviour
{
	public int startingHealth = 100;            // The amount of health the enemy starts the game with.
	public int currentHealth;                   // The current health the enemy has.
	public Slider healthSlider;                 // Reference to the UI's health bar.
	
	//public int scoreValue = 10;                 // The amount added to the player's score when the enemy dies.
	//public AudioClip deathClip;                 // The sound to play when the enemy dies.
	
	
	//Animator anim;                              // Reference to the animator.
	AudioSource enemyAudio;                    // Reference to the audio source.
	bool isDead;                                // Whether the enemy is dead.
	
	
	void Awake ()
	{
		// Setting up the references.
		//		anim = GetComponent <Animator> ();
				enemyAudio = GetComponent <AudioSource> ();
		//		hitParticles = GetComponentInChildren <ParticleSystem> ();
		//		capsuleCollider = GetComponent <CapsuleCollider> ();
		
		// Setting the current health when the enemy first spawns.
		currentHealth = startingHealth;
	}
	
	void Update ()
	{
		
	}
	
	
	public void TakeDamage (int amount)
	{
		// If the enemy is dead...
		if(isDead)
			// ... no need to take damage so exit the function.
			return;
		
		// Play the hurt sound effect.
		enemyAudio.Play ();
		
		// Reduce the current health by the amount of damage sustained.
		currentHealth -= amount;
		
		// Set the health bar's value to the current health.
		healthSlider.value = currentHealth;
		
		// If the current health is less than or equal to zero...
		if(currentHealth <= 0)
		{
			// ... the enemy is dead.
			Death ();
			Application.LoadLevel ("Win");
		}
	}
	
	
	void Death ()
	{
		// The enemy is dead.
		isDead = true;
		
		// Tell the animator that the enemy is dead.
		//anim.SetTrigger ("Dead");
		
		// Change the audio clip of the audio source to the death clip and play it (this will stop the hurt clip playing).
		//enemyAudio.clip = deathClip;
		//enemyAudio.Play ();
	}
	
}
﻿using UnityEngine;
using System.Collections;

public class MenuMusic : MonoBehaviour {

	void Awake() {
		GameObject gameMusic = GameObject.Find("GameMusic");
		if (gameMusic) {
			// kill game music
			Destroy(gameMusic);
		}
		DontDestroyOnLoad(this.gameObject);
	}
}
﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CameraController : MonoBehaviour {
	public Text timer;
	float timeLeft = 100.0f;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if (timeLeft >= 0) {
			timeLeft -= Time.deltaTime;
			timer.text = ((int)timeLeft).ToString();		
		}
		else{
			Application.LoadLevel("Loss");//change to lose page... 
		}
	}
}

﻿using UnityEngine;
using System.Collections;

public class MusicSingleton : MonoBehaviour {

	void Awake() {
		GameObject menuMusic = GameObject.Find("MenuMusic");
		if (menuMusic) {
			// kill menu music
			Destroy(menuMusic);
		}
		DontDestroyOnLoad(this.gameObject);
	}
}